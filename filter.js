const filterr = (elements,cb) => {                             // declaring the function
    const newArr = []                                          // array to accumulate the result
    
    if (elements !== [] && Array.isArray(elements)) {          // to check the type of input
    for (let item of elements) {                               // iterating the elements of array[input]
        if (cb(item)) {
            newArr.push(item)                                  // pushing the filtered element into new array
        }
    } return newArr                                            // returning the filtered array
    } else {
        return 'Invalid input'                                 // incase the input is of another type
    } 
}

module.exports = filterr                                       // exporting the function
