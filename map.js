const maps = (elements,cb) => {                             // declaring the function
    const newArr = []                                       // to accumulate the array
    
    if (Array.isArray(elements)) {                          // to check the input is array
    for (let element of elements) {                         // iterating through the array
        let result = cb(element)                            // calling the callback function
        newArr.push(result)                                 // pushing the result into emoty array
    } return newArr
    } else {
        return 'Invalid input'
    }
}

module.exports = maps                                       // exporting the array

