const reducee = (elements,cb,start_value=0) => {                     // declaring the function
    let result                                                       // declaring a new variable
    
    if (Array.isArray(elements)) {                                   // to check if the input is array
    for (let item of elements) {                                     // iterating the array
        result = cb(start_value,item)                                // calling the calback function
        start_value = result
    } return result
    } else {
        return 'Invalid input'
    }
}

module.exports = reducee                                             // exporting the function