function each (elements,cb) {                                  // function has been declared

    if (Array.isArray(elements)) {                             // checking whether the input is array or not

    for (let item in elements) {                               // iterating through the array elements
        cb(elements[item],item)                                // invoking the callback function
    }
    } else {
        return 'Invalid input'                                 // incase if the input is of other type
    }
}

module.exports = each                                          // exporting the function