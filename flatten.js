

const array_check = item => {                                    // array to convert the array into number
    for (let element of item) {                                 
        if (!Array.isArray(element)) {
            return element
        } else {
            return array_check(element) 
        }
    }
}

const flat = elements => {                                       // declaring the flatten function
    let new_array = []                                           // to accumulate the result

    if (Array.isArray(elements)) {                               // to check if the input is array
    for (let element of elements) {                              // iterating the array
        if (!Array.isArray(element)) {                           // to check if the iterated element is array 
            new_array.push(element)                              // pushing the number into empty array
        } else {
            new_array.push(array_check(element))                 // invoking the array_check function to convert array number into number
        }
    } return new_array                                           // returning the result
    } else {
        return 'Invalid input'
    } 
}


module.exports = flat                                            // exporting the array





