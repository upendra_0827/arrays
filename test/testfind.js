const function_find = require('../find.js')                                // importing the function

const items = [1, 2, 3, 4, 5, 5];

const result_element_1 = function_find(items,funct = item => {                          // checking the function 
    return item>3
})

console.log(result_element_1)


const result_element_2 = function_find(false,funct = item => {                          // test case 2
    return item>3
})

console.log(result_element_2)


const result_element_3 = function_find(0,funct = item => {                          // test case 3
    return item>3
})

console.log(result_element_3)


const result_element_4 = function_find('hi',funct = item => {                          // test case 4
    return item>3
})

console.log(result_element_4)