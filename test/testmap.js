const items = [1, 2, 3, 4, 5, 5];

const function_map = require('../map.js')                              // importing the array

const new_array_1 = function_map(items,functionn = item => {             // checking the function
    return item*15
})

console.log(new_array_1)


const new_array_2 = function_map('hi',functionn = item => {             // test case 2
    return item*15
})

console.log(new_array_2)


const new_array_3 = function_map(0,functionn = item => {             // test case 3
    return item*15
})

console.log(new_array_3)

