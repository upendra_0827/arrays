const function_reduce = require('../reduce.js')                                // importing the array 

const items = [1, 2, 3, 4, 5, 5];

const result_value_1 = function_reduce(items,(funct = (num_1,num_2) => {        // checking the function
    return num_1+num_2
}))

console.log(result_value_1)


const result_value_2 = function_reduce('hi',(funct = (num_1,num_2) => {        // test case 2
    return num_1+num_2
}))

console.log(result_value_2)


const result_value_3 = function_reduce(0,(funct = (num_1,num_2) => {        // test case 3
    return num_1+num_2
}))

console.log(result_value_3)


const result_value_4 = function_reduce(true,(funct = (num_1,num_2) => {        // test case 4
    return num_1+num_2
}))

console.log(result_value_4)