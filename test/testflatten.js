
const function_flatten = require('../flatten.js')                     // importing the function

const nestedArray = [1, [2], [[3]], [[[4]]]]

const new_array = function_flatten(nestedArray)                       // testing the function

console.log(new_array) 

console.log(function_flatten('hi'))                                   // test case 2
console.log(function_flatten(true))                                  // test case 3
console.log(function_flatten(0))                                      // test case 4