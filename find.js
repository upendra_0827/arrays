const findd = (elements,cb) => {                             // declaring the function

    if (Array.isArray(elements)) {                           // checking if the input is array
    for (let element of elements) {                          // iterating the elements
        if (cb(element)) {                              
            return element                                   // returning the element
        }
    }} else {
        return 'Invalid input'                               // incase if the input is of other type
    }
}

module.exports = findd                                       // exporting the function

